# Contacts App
A simple contacts management app developed using 
- Spring-boot
- Mongo-DB
  
[![Build Status](https://travis-ci.com/chejerlakarthik/contacts-app.svg?branch=master)](https://travis-ci.com/chejerlakarthik/contacts-app) 
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=chejerlakarthik_contacts-app&metric=alert_status)](https://sonarcloud.io/dashboard?id=chejerlakarthik_contacts-app)